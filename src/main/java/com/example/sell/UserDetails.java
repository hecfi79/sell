package com.example.sell;

import java.util.Objects;

public class UserDetails {
    private String id;

    private String email;

    private String password;

    private String role;

    private String name;

    private String surname;

    private String father;

    private String telephoneNumber;

    private String login;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public boolean setEmail(String email) {
        if (email.matches("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")) {
            this.email = email;
            return true;
        } else {
            System.out.println("Введенная почта некорректна");
            return false;
        }
    }

    public String getPassword() {
        return password;
    }


    /**
     * @param password - пароль от аккаунта
     * @return tag - верно ли введен пароль
     * Объяснение по регулярным выражениям:
     * ^ - начало строки;
     * (?=.*[0-9]) - цифра хотя бы раз должна встретиться;
     * (?=.*[a-z]) - строчная буква должна встретиться хотя бы раз;
     * (?=.*[A-Z]) - заглавная буква должна встретиться хотя бы раз;
     * (?=.*[@#$%^&+=]) - специальный символ, из тех, что в [], долженн встретиться хотя бы раз
     * (?=\S+$) - пробелы по всей строке недопустимы
     * .{8,} - не менее восьми символов
     * $ - конец строки
     */
    public boolean setPassword(String password) {
        if (password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
            this.password = password;
            return true;
        } else {
            System.out.println("Введенный пароль некорректен");
            return false;
        }
    }

    public String getRole() {
        return role;
    }

    public boolean setRole(String role) {
        if (role.equals("guest") || role.equals("admin")) {
            this.role = role;
            return true;
        } else {
            System.out.println("Введенная роль некорректна");
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public boolean setName(String name) {
        if (name.matches("^[\\p{L} .'\\-]+$")) {
            this.name = name;
            return true;
        } else {
            System.out.println("Введенное имя некорректно");
            return false;
        }
    }

    public String getSurname() {
        return surname;
    }

    public boolean setSurname(String surname) {
        if (surname.matches("^[\\p{L} .'\\-]+$")) {
            this.surname = surname;
            return true;
        } else {
            System.out.println("Введенная фамилия некорректна");
            return false;
        }
    }

    public String getFather() {
        return father;
    }

    public boolean setFather(String father) {
        if (father.matches("^[\\p{L} .'\\-]+$")) {
            this.father = father;
            return true;
        } else {
            System.out.println("Введенное отчество некорректно");
            return false;
        }
    }

    public String getLogin() {
        return login;
    }

    /**
     * @param login - login
     * @return boolean - соответствует ли наименование регулярному выражению
     *
     * формат записи логина: руслан12 или heckfy12, можно с заглавной
     */
    public boolean setLogin(String login) {
        if (login.matches("^[a-zа-яA-ZА-Я](.[a-zа-яA-Z0-9_-А-Я0-9_-]*)$")) {
            this.login = login;
            return true;
        } else {
            System.out.println("Введенный логин некорректно");
            return false;
        }
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * @param telephoneNumber - номер, начинающийся с девятки: 9123456789
     * [9]{1} – указывает на то, что первым символом будет цифра 9.
     * [0-9]{9} – позволяет ввести 9 цифр.
     */
    public boolean setTelephoneNumber(String telephoneNumber) {
        if (telephoneNumber.matches("^([9]{1}[0-9]{9})?$")) {
            this.telephoneNumber = telephoneNumber;
            return true;
        } else {
            System.out.println("Введенный телефон некорректен.");
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDetails userDetails = (UserDetails) o;

        return Objects.equals(id, userDetails.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return getId() + " "
                + getName() + " "
                + getSurname() + " "
                + getFather() + " "
                + getTelephoneNumber() + " "
                + getEmail() + " "
                + getLogin() + " "
                + getPassword() + " "
                + getRole();
    }
}
