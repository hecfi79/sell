package com.example.sell;

import java.util.Objects;

public class Product {
    private String id;

    private String name;

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    /**
     * @param name - наименование товара
     * @return boolean - соответствует ли наименование регулярному выражению
     *
     * формат записи логина: яблоко1 (например, красное) или яблоко2 (например, зеленое), можно с заглавной
     */
    public boolean setName(String name) {
        if (name.matches("^[a-zа-яA-ZА-Я](.[a-zа-яA-Z0-9_-А-Я0-9_-]*)$")) {
            this.name = name;
            return true;
        } else {
            System.out.println("Введенное наименование товара некорректно");
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return getId() + " " + getName();
    }
}
