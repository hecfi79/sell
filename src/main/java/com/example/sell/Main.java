package com.example.sell;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    public static int id = 1;
    public static int product_id = 0;

    public static void deleteNullsUsers(ArrayList<UserDetails> userDetailsArrayList) {
        userDetailsArrayList.removeIf(value -> value.getLogin() == null || value.getName() == null
                || value.getSurname() == null || value.getFather() == null
                || value.getTelephoneNumber() == null || value.getEmail() == null);
    }

    public static void deleteNullsProducts(ArrayList<Product> products) {
        products.removeIf(value -> value.getName() == null);
    }
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        ArrayList<UserDetails> userDetailsArrayList = new ArrayList<>();
        UserDetails admin = new UserDetails();
        admin.setId("0");
        admin.setName("admin");
        admin.setSurname("admin");
        admin.setFather("admin");
        admin.setTelephoneNumber("9999999999");
        admin.setEmail("admin@gmail.com");
        admin.setPassword("Administrator0#");
        admin.setRole("admin");
        userDetailsArrayList.add(admin);
        ArrayList<Product> products = new ArrayList<>();
        while (true) {
            System.out.println("1) регистрация;\n2) вход;");
            switch (in.nextLine()) {
                case "1":
                    UserDetails userDetails = new UserDetails();
                    userDetails.setId(String.valueOf(id));
                    System.out.println("Введите имя:");
                    userDetails.setName(in.nextLine());
                    System.out.println("Введите фамилию:");
                    userDetails.setSurname(in.nextLine());
                    System.out.println("Введите отчество:");
                    userDetails.setFather(in.nextLine());
                    System.out.println("Введите номер телефона:");
                    userDetails.setTelephoneNumber(in.nextLine());
                    System.out.println("Введите почту:");
                    userDetails.setEmail(in.nextLine());
                    System.out.println("Введите логин:");
                    userDetails.setLogin(in.nextLine());
                    System.out.println("Введите пароль:");
                    userDetails.setPassword(in.nextLine());
                    userDetails.setRole("guest");
                    userDetailsArrayList.add(userDetails);
                    id++;
                    break;

                case "2":
                    System.out.println("Введите почту:");
                    String email = in.nextLine();
                    System.out.println("Введите пароль:");
                    String password = in.nextLine();
                    Boolean isEntered = false, isAdmin = false, isUser = false;
                    for (UserDetails u : userDetailsArrayList) {
                        if (email.equals(u.getEmail())) {
                            if (password.equals(u.getPassword())) {
                                isEntered = true;
                                isAdmin = "admin".equals(u.getRole());
                                isUser = "guest".equals(u.getRole());
                            } else {
                                System.out.println("Пароль введен неверно.");
                                break;
                            }
                        }
                    }
                    if (isEntered) {
                        while (true) {
                            switch (String.valueOf(isAdmin)) {
                                case "true":
                                    System.out.println("Вы зашли как администратор. Что Вы желаете сделать?");
                                    System.out.println("1) изменить роль пользователя по id\n2) показать всех пользователей\n3) добавить товар\n4) удалить товар\n5) просмотреть товары\n6) выйти");
                                    switch (in.nextLine()) {
                                        case "1":
                                            System.out.println("Введите id пользователя");
                                            String user_id = in.nextLine();
                                            System.out.println("Вы уверены?\n1) да\n2) нет");
                                            switch (in.nextLine()) {
                                                case "1":
                                                    String role = "";
                                                    for (UserDetails user : userDetailsArrayList) {
                                                        if (user_id.equals(user.getId()) && !user_id.equals("0")) {
                                                            if (user.getRole().equals("guest")) {
                                                                user.setRole("admin");
                                                                role = "admin";
                                                            } else {
                                                                user.setRole("guest");
                                                                role = "guest";
                                                            }
                                                        }
                                                    }
                                                    System.out.println("Роль пользователю с id = " + user_id + " изменена на " + role);
                                                    break;

                                                case "2":
                                                    System.out.println("Выходим...");
                                                    break;
                                            }
                                            break;

                                        case "2":
                                            deleteNullsUsers(userDetailsArrayList);
                                            userDetailsArrayList.forEach((value) -> System.out.println(value.toString()));
                                            break;

                                        case "3":
                                            Product product = new Product();
                                            System.out.println("Введите наименование товара:");
                                            product.setId(String.valueOf(product_id));
                                            product.setName(in.nextLine());
                                            products.add(product);
                                            product_id += 1;
                                            break;

                                        case "4":
                                            System.out.println("Введите id товара, который хотите удалить:");
                                            if (!products.isEmpty()) {
                                                products.remove(in.nextLine());
                                            } else {
                                                System.out.println("Список товаров пуст.");
                                            }
                                            break;

                                        case "5":
                                            deleteNullsProducts(products);
                                            products.forEach((value) -> System.out.println(value.toString()));
                                            break;

                                        case "6":
                                            System.out.println("Выходим...");
                                            break;
                                    }
                                    break;

                                case "false":
                                    break;
                            }
                            switch (String.valueOf(isUser)) {
                                case "true":
                                    System.out.println("Вы зашли как пользователь. Выберите действие:\n1) просмотреть товары\n2) выйти");
                                    switch (in.nextLine()) {
                                        case "1":
                                            deleteNullsProducts(products);
                                            products.forEach((value) -> System.out.println(value.toString()));
                                            break;

                                        case "2":
                                            break;
                                    }
                                    break;

                                case "false":
                                    break;
                            }
                            break;
                        }
                    } else {
                        System.out.println("Данные введены некорректно. Попробуйте еще раз.");
                    }
                    break;

                case "3":
                    break;
            }
        }
    }
}