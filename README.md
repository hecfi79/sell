# Sell



## Начало работы

Для того, чтобы запустить проект, необходимо перейти в [GitLab main-branch](https://gitlab.com/hecfi79/sell)
и скачать [архив с проектом](https://gitlab.com/hecfi79/sell/-/archive/main/sell-main.zip).

## Установка программ для Windows (!!!)

Необходимо установить [Java SE Development Kit](https://softslot.ru/d28bc0c/programming/other/jdk-11_windows-x64_bin.exe) и
[IntelliJ IDEA Community](https://jetbrains.com/ru-ru/idea/download/download-thanks.html?platform=windows&code=IIC) 
для продолжения работы.

## После установки программ

- Создайте папку с названием sell;
- Распакуйте в нее содержимое архива;
- Откройте папку sell c помощью IntelliJ IDEA Community;
- Подождите немного, после чего запустите основной класс Main.

## Обратная связь

В случае возникновения проблем, пожалуйста, обратитесь ко мне в личные сообщения на один из сервисов: 
- [Kwork](https://kwork.ru/user/ruslangulyanovlive);
- Discord: Hecfi#4004;
- gmail: ruslangulyanov.live@gmail.com.